package com.devcamp.arrayintegerfilterapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

@Service
public class ArrayFilterService {
    private final int[] arrayNums = {1, 23, 32, 43, 54, 65, 86, 10, 15, 16, 18};

    public ArrayList<Integer> getLargerNumbers(int pos) {
        ArrayList<Integer> largerNumbers = new ArrayList<>();
        for (int num : arrayNums) {
            if (num > pos) {
                largerNumbers.add(num);
            }
        }
        return largerNumbers;
    }

    public Integer getNumberAtIndex(int index) {
        if (index < 0 || index >= arrayNums.length) {
            return null;
        }
        return arrayNums[index];
    }
}
