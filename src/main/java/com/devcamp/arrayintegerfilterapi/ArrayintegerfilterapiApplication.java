package com.devcamp.arrayintegerfilterapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArrayintegerfilterapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArrayintegerfilterapiApplication.class, args);
	}

}
