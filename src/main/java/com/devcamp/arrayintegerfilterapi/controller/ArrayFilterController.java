package com.devcamp.arrayintegerfilterapi.controller;


import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.arrayintegerfilterapi.service.ArrayFilterService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class ArrayFilterController {
    @Autowired
    private ArrayFilterService arrayFilterService;

    @GetMapping("/array-int-request-query")
    public ArrayList<Integer> getLargerNumbers(@RequestParam int pos) {
        return arrayFilterService.getLargerNumbers(pos);
    }

    @GetMapping("/array-int-param/{index}")
    public Integer getNumberAtIndex(@PathVariable int index) {
        return arrayFilterService.getNumberAtIndex(index);
    }
}
